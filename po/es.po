# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# acutbal <acutbal@gmail.com>, 2018
# Casper casper, 2020
# Jaime Marquínez Ferrándiz, 2017-2018
# prflr88 <prflr88@gmail.com>, 2017-2018
# prflr88 <prflr88@gmail.com>, 2017
# Toni Estévez <toni.estevez@gmail.com>, 2021
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-29 18:32+0200\n"
"PO-Revision-Date: 2021-05-23 13:41+0000\n"
"Last-Translator: Toni Estévez <toni.estevez@gmail.com>\n"
"Language-Team: Spanish (http://www.transifex.com/xfce/xfce-panel-plugins/language/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:1
msgid "PulseAudio Plugin"
msgstr "PulseAudio"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:2
#: ../panel-plugin/pulseaudio-plugin.c:242
msgid "Adjust the audio volume of the PulseAudio sound system"
msgstr "Ajusta el volumen del sistema de sonido PulseAudio"

#: ../panel-plugin/pulseaudio-dialog.glade.h:1
msgid "PulseAudio Panel Plugin"
msgstr "Complemento para PulseAudio"

#: ../panel-plugin/pulseaudio-dialog.glade.h:2
msgid "Enable keyboard _shortcuts for volume control"
msgstr "Activar los _atajos de teclado para el control del volumen"

#: ../panel-plugin/pulseaudio-dialog.glade.h:3
msgid ""
"Enables volume control using multimedia keys. Make sure no other application"
" that listens to these keys (e.g. xfce4-volumed) is running in the "
"background."
msgstr "Activa el control del volumen mediante las teclas multimedia. Asegúrese de que no se está ejecutando en segundo plano ninguna otra aplicación que responda a esas teclas (por ejemplo, xfce4-volumed)."

#: ../panel-plugin/pulseaudio-dialog.glade.h:4
msgid "Show _notifications when volume changes"
msgstr "Mostrar _notificaciones al cambiar el volumen"

#: ../panel-plugin/pulseaudio-dialog.glade.h:5
msgid "Enables on-screen volume notifications."
msgstr "Activa las notificaciones del volumen en pantalla."

#: ../panel-plugin/pulseaudio-dialog.glade.h:6
msgid "Behaviour"
msgstr "Comportamiento"

#: ../panel-plugin/pulseaudio-dialog.glade.h:7
msgid "Audio _Mixer"
msgstr "_Mezclador de audio"

#: ../panel-plugin/pulseaudio-dialog.glade.h:8
msgid ""
"Audio mixer command that can be executed from the context menu, e.g. "
"\"pavucontrol\", \"unity-control-center sound\"."
msgstr "Orden del mezclador de audio que se puede ejecutar desde el menú contextual, por ejemplo, «pavucontrol»."

#: ../panel-plugin/pulseaudio-dialog.glade.h:9
msgid "_Run Audio Mixer..."
msgstr "_Ejecutar el mezclador de audio..."

#: ../panel-plugin/pulseaudio-dialog.glade.h:10
msgid "Sound Settings"
msgstr "Configuración del sonido"

#: ../panel-plugin/pulseaudio-dialog.glade.h:11
msgid "General"
msgstr "General"

#: ../panel-plugin/pulseaudio-dialog.glade.h:12
msgid "Control Playback of Media Players"
msgstr "Controlar la reproducción de los reproductores multimedia"

#: ../panel-plugin/pulseaudio-dialog.glade.h:13
msgid "Enable multimedia keys for playback control"
msgstr "Activar las teclas multimedia para el control de la reproducción"

#: ../panel-plugin/pulseaudio-dialog.glade.h:14
msgid "Enable experimental window focus support"
msgstr "Activar la compatibilidad experimental con el enfoque de la ventana"

#: ../panel-plugin/pulseaudio-dialog.glade.h:15
msgid "Title"
msgstr "Título"

#: ../panel-plugin/pulseaudio-dialog.glade.h:16
msgid "Hidden"
msgstr "Ocultar"

#: ../panel-plugin/pulseaudio-dialog.glade.h:17
msgid "Clear Known Items"
msgstr "Borrar los elementos conocidos"

#: ../panel-plugin/pulseaudio-dialog.glade.h:18
msgid "Please restart your panel for additional players to be displayed."
msgstr "Reinicie el panel para que se muestren reproductores adicionales."

#: ../panel-plugin/pulseaudio-dialog.glade.h:19
msgid "Known Media Players"
msgstr "Reproductores multimedia conocidos"

#: ../panel-plugin/pulseaudio-dialog.glade.h:20
msgid "Media Players"
msgstr "Reproductores multimedia"

#: ../panel-plugin/pulseaudio-plugin.c:244
msgid "Copyright © 2014-2020 Andrzej Radecki et al.\n"
msgstr "Copyright © 2014-2020 Andrzej Radecki y otros\n"

#: ../panel-plugin/pulseaudio-dialog.c:142
#: ../panel-plugin/pulseaudio-menu.c:264
#, c-format
msgid ""
"<big><b>Failed to execute command \"%s\".</b></big>\n"
"\n"
"%s"
msgstr "<big><b>Error al ejecutar la orden «%s».</b></big>\n\n%s"

#: ../panel-plugin/pulseaudio-dialog.c:145
#: ../panel-plugin/pulseaudio-menu.c:267
msgid "Error"
msgstr "Error"

#: ../panel-plugin/pulseaudio-button.c:333
#: ../panel-plugin/pulseaudio-notify.c:199
#, c-format
msgid "Not connected to the PulseAudio server"
msgstr "Sin conexión con el servidor PulseAudio"

#: ../panel-plugin/pulseaudio-button.c:335
#, c-format
msgid "Volume %d%% (muted)"
msgstr "Volumen: %d%% (silenciado)"

#: ../panel-plugin/pulseaudio-button.c:337
#, c-format
msgid "Volume %d%%"
msgstr "Volumen: %d%%"

#: ../panel-plugin/pulseaudio-menu.c:507
msgid "Output"
msgstr "Salida"

#: ../panel-plugin/pulseaudio-menu.c:546
msgid "Input"
msgstr "Entrada"

#: ../panel-plugin/pulseaudio-menu.c:634
msgid "Choose Playlist"
msgstr "Elija la lista de reproducción"

#. Audio mixers
#: ../panel-plugin/pulseaudio-menu.c:671
msgid "_Audio mixer..."
msgstr "Mezclador de _audio..."

#: ../panel-plugin/pulseaudio-notify.c:201
#, c-format
msgid "Volume %d%c (muted)"
msgstr "Volumen: %d%c (silenciado)"

#: ../panel-plugin/pulseaudio-notify.c:203
#, c-format
msgid "Volume %d%c"
msgstr "Volumen: %d%c"

#: ../panel-plugin/mprismenuitem.c:363 ../panel-plugin/mprismenuitem.c:436
#: ../panel-plugin/mprismenuitem.c:810
msgid "Not currently playing"
msgstr "No se está reproduciendo"
